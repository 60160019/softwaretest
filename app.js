const math = require('./math');
const assert = require('assert');
describe('file to be tested', () => {
    context('function to be tested', () => {
        it('should do something', () => {
            assert.equal(1, 1);
        });
        it('should do another thing', () => {
            assert.equal("aya", "aya");
            assert.deepEqual({ name: "aya" }, { name: "aya" });
        });
    });
});

describe('file math.js', () => {
    context('function add1', () => {
        it('should do add1(1,2) equal 3', () => {
            assert.equal(math.add1(1, 2), 3);
        });
    });
});

