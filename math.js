const add1 = (n1, n2) => { return (n1 + n2) };
const add2 = add1;
const add3 = add1;
module.exports = {
    add1,
    add2,
    add3
}