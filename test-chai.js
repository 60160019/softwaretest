const chai = require('chai');
const math = require('./math');
const expect = chai.expect;

describe('Test chai', () => {
    it('should compare thing expect', () => {
        expect(1).to.equal(1);
    });
    it('compare math.js ', () => {
        expect(2).to.equal(math.add1(1, 1));
    });
    it('compare another things by expect', () => {
        expect(5 > 8).to.be.false;
        expect({ name: "bek" }).to.deep.equal({ name: "bek" });
        expect({ name: "bek" }).to.have.property("name").to.equal("bek");
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('bek').to.be.a('string');
        expect('bek'.length).to.equal(3);
        expect('bek').to.lengthOf(3);
        expect([1, 2, 3]).to.lengthOf(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});